# westpointrealtors-theme

Gem should auto-build and push to Rubygems upon commit.

Manual instructions  
To Build Image and Gem
```
docker build -t westpointrealtors-theme . && docker run --rm -v "$PWD:/srv" westpointrealtors-theme build westpointrealtors-theme.gemspec
```

To Push Gem and Clean Up
```
docker run --rm -v "$PWD:/srv" westpointrealtors-theme push westpointrealtors-theme-*.gem && rm westpointrealtors-theme-*.gem
```

Omit Build if no changes occurred to build files (Gemfile, westpointrealtors-theme.gemspec)
