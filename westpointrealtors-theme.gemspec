# coding: utf-8

Gem::Specification.new do |spec|
  spec.name          = "westpointrealtors-theme"
  spec.version       = "0.0.3"
  spec.authors       = ["Mario Lopez"]
  spec.email         = ["mario@techmunchies.net"]

  spec.summary       = %q{This is the official theme of westpointrealtors.com}
  spec.homepage      = "https://github.com/mariolopjr/westpointrealtors-theme"
  spec.license       = "MIT"

  spec.metadata["plugin_type"] = "theme"

  spec.files         = `git ls-files -z`.split("\x0").select do |f|
      f.match(%r{^(assets|_(includes|layouts|sass)/|(LICENSE|README)((\.(txt|md|markdown)|$)))}i)
  end

  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }


  spec.add_runtime_dependency "jekyll-seo-tag", "~> 2.0"

  spec.add_development_dependency "jekyll", "~> 3.3"
  spec.add_development_dependency "bundler", "~> 1.12"
  spec.add_development_dependency "rake", "~> 10.0"
end
