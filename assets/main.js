$(document).ready(function() {

  // Ensures that Galleria isn't loaded unless it exists!
  if (typeof Galleria !== 'undefined') {
    (function() {
      Galleria.run('.galleria');
    }());
  }
});
