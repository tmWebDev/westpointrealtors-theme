FROM mariolopjr/jekyll:latest
MAINTAINER mario@techmunchies.net

COPY Gemfile /
COPY westpointrealtors-theme.gemspec /

RUN set -ex \
  && apk add --no-cache git \
  && bundle install

VOLUME /srv

WORKDIR /srv
ENTRYPOINT ["gem"]
